// pages/contact_info_setting.js
let app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    countries: ["孩子/老人", "家长"],
    countryIndex: '',
    nickname: '',
    distancesize: '',
    guest_open_id: '',
    admin_flag: false,
    isdiabled:true

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    let that = this;
    var authorities = wx.getStorageSync('authorities');
    // if (authorities.indexOf("ROLE_NONE") >= 0) {
    //   that.dialog.show();
    // }
    if (authorities.indexOf("ROLE_ADMIN") >= 0) {
      that.setData({
        admin_flag: true,
        isdiabled:false
      })
    }
    that.setData({
      countryIndex: options.relationstate,
      nickname: options.nickname,
      distancesize: options.distancesize,
      guest_open_id: options.seconduseropenid
    });

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  },
  bindRelationshipChange: function(e) {
    console.log('picker country 发生选择改变，携带值为', e.detail.value);
    this.setData({
      countryIndex: e.detail.value
    })
  },
  bindNicknameInput: function(e) {
    this.setData({
      nickname: e.detail.value
    })
  },
  bindDistanceInput: function(e) {
    this.setData({
      distancesize: e.detail.value
    })
  },
  saveButton: function() {
    let that = this;
    wx.request({
      url: app.globalData.url + '/relationships',
      method: 'PUT',
      header: {
        'Authorization': 'Bearer ' + wx.getStorageSync('id_token'),
        'content-type': 'application/x-www-form-urlencoded'
      },
      data: {
        main_open_id: wx.getStorageSync('open_id'),
        second_open_id: that.data.guest_open_id,
        relation: that.data.countryIndex,
        distance: that.data.distancesize,
        nickname: that.data.nickname,
      },
      success: res => {
        console.log("更新用户间关系")
        console.log(res)
        wx.showToast({
          title: '已完成',
          icon: 'success',
          duration: 3000
        });
      },
      fail: err => {
        wx.showToast({
          title: '失败',
          icon: 'error',
          duration: 3000
        });
      }
    });




  }
})