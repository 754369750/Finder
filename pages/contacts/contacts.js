// pages/contacts/contacts.js
let app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    friendList: []

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    // let that = this;
    // that.getFriendList();

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
    let that = this;
    that.getFriendList();

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  },
  updateSet: function(e) {
    var id = e.currentTarget.dataset.id;
    var nickname = e.currentTarget.dataset.nickname;
    var distancesize = e.currentTarget.dataset.distancesize;
    var relationstate = e.currentTarget.dataset.relationstate;
    var seconduseropenid = e.currentTarget.dataset.seconduseropenid;



    wx.navigateTo({
      title: "详细信息",
      url: '../contact_info_setting/contact_info_setting?id=' + id + '&nickname=' + nickname + '&distancesize=' + distancesize + '&relationstate=' + relationstate + '&seconduseropenid=' + seconduseropenid
    })
  },
  getFriendList:function(){
    let that = this;
    wx.request({
      url: app.globalData.url + '/relationships',
      method: 'GET',
      header: {
        'Authorization': 'Bearer ' + wx.getStorageSync('id_token'),
        'content-type': 'application/x-www-form-urlencoded'
      },
      data: {
        main_open_id: wx.getStorageSync('open_id')
      },
      success: res => {
        console.log("获取用户的朋友列表")
        console.log(res)
        if(res.data.length>0){
          let list = [];
          for (var i = 0; i < res.data.length; i++) {
            list.push({
              headimage: res.data[i].headImage,
              name: res.data[i].userName,
              relationstate: res.data[i].relationship.relation,
              id: res.data[i].relationship.id,
              nickname: res.data[i].relationship.nickname,
              distancesize: res.data[i].relationship.distanceSize,
              secondUserOpenId: res.data[i].relationship.secondUserOpenId
            })
          }
          that.setData({
            friendList: list
          })

        }
       }
    });


  }
})