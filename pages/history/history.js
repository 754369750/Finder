// pages/history/history.js
let app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    historyList: []

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    let that = this;
    that.getHistoryList();
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {


  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  },
  getHistoryDetail: function(e) {

    var start_time = e.currentTarget.dataset.starttime;
    var end_time = e.currentTarget.dataset.endtime;
    var distance = e.currentTarget.dataset.distance;
    var taskid = e.currentTarget.dataset.taskid;
    var warningid = e.currentTarget.dataset.warningid;
    wx.navigateTo({
      title: "监控详情",
      url: '../hisroty_detail/hisroty_detail?taskid=' + taskid + '&warningid=' + warningid + '&start_time=' + start_time + '&end_time=' + end_time + '&distance=' + distance
    })
  },
  getHistoryList: function() {
    let that = this;
    if (wx.getStorageSync('authorities').indexOf("ROLE_ADMIN") > -1) {
      wx.request({
        url: app.globalData.url + '/check-tasks/admin',
        method: 'GET',
        header: {
          'Authorization': 'Bearer ' + wx.getStorageSync('id_token'),
          'content-type': 'application/x-www-form-urlencoded'
        },
        data: {
          admin_open_id: wx.getStorageSync('open_id')

        },
        success: res => {
          console.log("获取监控人的监控任务列表");
          console.log(res)
          if (res.data.length > 0) {
            let list = [];
            for (var i = 0; i < res.data.length; i++) {
              let start_time = res.data[i].checkTask.startTime;
              let end_time = res.data[i].checkTask.endTime;
              if (start_time != null) {
                start_time = start_time.replace('T', ' ');
                start_time = start_time.substring(0, 19);
              }
              if (end_time != null) {
                end_time = end_time.replace('T', ' ');
                end_time = end_time.substring(0, 19);
              } else {
                end_time = '本任务尚未结束';
              }
              list.push({
                headimage: res.data[i].headImage,
                name: res.data[i].userName,
                taskid: res.data[i].checkTask.id,
                starttime: start_time,
                endtime: end_time,
                distance: res.data[i].checkTask.distance,
                warningid: res.data[i].checkTask.warningId
              })
            }
            that.setData({
              historyList: list
            })
          }
        }
      })

    } else {

      wx.request({
        url: app.globalData.url + '/check-tasks/guest',
        method: 'GET',
        header: {
          'Authorization': 'Bearer ' + wx.getStorageSync('id_token'),
          'content-type': 'application/x-www-form-urlencoded'
        },
        data: {
          guest_open_id: wx.getStorageSync('open_id')

        },
        success: res => {
          console.log("获取监控人的监控任务列表");
          console.log(res)
          if (res.data.length > 0) {
            let list = [];
            for (var i = 0; i < res.data.length; i++) {
              let start_time = res.data[i].checkTask.startTime;
              let end_time = res.data[i].checkTask.endTime;
              if (start_time != null) {
                start_time = start_time.replace('T', ' ');
                start_time = start_time.substring(0, 19);
              }
              if (end_time != null) {
                end_time = end_time.replace('T', ' ');
                end_time = end_time.substring(0, 19);
              }
              list.push({
                headimage: res.data[i].headImage,
                name: res.data[i].userName,
                taskid: res.data[i].checkTask.id,
                starttime: start_time,
                endtime: end_time,
                distance: res.data[i].checkTask.distance,
                warningid: res.data[i].checkTask.warningId
              })
            }
            that.setData({
              historyList: list
            })
          }
        }
      })

    }




  }
})