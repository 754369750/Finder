// pages/contacts/contacts.js
let app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    warningList: []

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {

    var that = this;
    app.warnwatch(that.watchBack);
    that.getAdminMessageList();

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
    // wx.onSocketMessage(function(message) {
    //   console.log(message)
    // })
    let that = this;
    that.getAdminMessageList();

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  },
  getRouteDetail: function(e) {
    let that = this;
    var taskid = e.currentTarget.dataset.taskid;
    var warningid = e.currentTarget.dataset.warningid;
    var readflag = e.currentTarget.dataset.readflag;
    if (readflag == "0") {
      that.updateReadState(warningid);
    }
    console.log(taskid)
    console.log(warningid)
    wx.navigateTo({
      title: "路径回放",
      url: '../route_playback/route_playback?taskid=' + taskid
    })
  },
  watchBack: function(warning) {
    let that = this;
    console.log(22222);
    console.log('this.warning==' + warning);
    var taskId = warning.split(";")[0];
    var message = warning.split(";")[1];
    that.getMessageList(taskId);


  },
  getMessageList: function(taskid) {
    let that = this;
    wx.request({
      url: app.globalData.url + '/warnings/taskid',
      method: 'GET',
      header: {
        'Authorization': 'Bearer ' + wx.getStorageSync('id_token'),
        'content-type': 'application/x-www-form-urlencoded'
      },
      data: {
        taskid: taskid
      },
      success: res => {
        console.log("获取警告信息成功");
        console.log(res)
        var oldlist = that.data.warningList;
        var flag = '0';
        console.log(oldlist)
        if (oldlist.length > 0) {
          for (var i = 0; i < oldlist.length; i++) {
            if (oldlist[i].openid == res.data.openid) {
              oldlist[i].message = res.data.message;
              oldlist[i].warningid = res.data.warningid;
              oldlist[i].readflag = res.data.readflag;
              flag = '1';
            }
          }
          if (flag == '0') {
            oldlist.push({
              headimage: res.data.headimage,
              name: res.data.name,
              taskid: res.data.taskid,
              openid: res.data.openid,
              message: res.data.message,
              readflag: res.data.readflag,
              warningid: res.data.warningid
            })

          }
          that.setData({
            warningList: oldlist
          })

        } else {

          let list = [];
          list.push({
            headimage: res.data.headimage,
            name: res.data.name,
            taskid: res.data.taskid,
            openid: res.data.openid,
            message: res.data.message,
            readflag: res.data.readflag,
            warningid: res.data.warningid
          })

          that.setData({
            warningList: list
          })
        }

        if (that.data.warningList.length > 0) {
          var isread = '0'
          for (var j = 0; j < that.data.warningList.length; j++) {
            if (that.data.warningList[j].readflag == '0') {
              isread = '1';
            }
          }
          if (isread == '1') {
            wx.showTabBarRedDot({
              index: 1
            })

          } else {
            wx.hideTabBarRedDot({
              index: 1
            })
          }


        }

      }





    })

  },
  getAdminMessageList: function() {
    let that = this;
    wx.request({
      url: app.globalData.url + '/warnings/adminopenid',
      method: 'GET',
      header: {
        'Authorization': 'Bearer ' + wx.getStorageSync('id_token'),
        'content-type': 'application/x-www-form-urlencoded'
      },
      data: {
        admin_open_id: wx.getStorageSync('open_id')
      },
      success: res => {
        console.log("获取警告信息成功");
        console.log(res)
        if (res.data.length > 0) {
          let list = [];
          for (var i = 0; i < res.data.length; i++) {
            list.push({
              headimage: res.data[i].headimage,
              name: res.data[i].name,
              taskid: res.data[i].taskid,
              openid: res.data[i].openid,
              message: res.data[i].message,
              readflag: res.data[i].readflag,
              warningid: res.data[i].warningid
            })
          }
          that.setData({
            warningList: list
          })
        }
        if (that.data.warningList.length > 0) {
          var isread = '0'
          for (var j = 0; j < that.data.warningList.length; j++) {
            if (that.data.warningList[j].readflag == '0') {
              isread = '1';
            }
          }
          if (isread == '1') {
            wx.showTabBarRedDot({
              index: 1
            })

          } else {
            wx.hideTabBarRedDot({
              index: 1
            })
          }


        }
      }
    })
  },
  updateReadState: function(warningid) {
    wx.request({
      url: app.globalData.url + '/warnings/readflag',
      method: 'PUT',
      header: {
        'Authorization': 'Bearer ' + wx.getStorageSync('id_token'),
        'content-type': 'application/x-www-form-urlencoded'
      },
      data: {
        warningid: warningid
      },
      success: res => {
        console.log("修改消息为读取状态成功");
      }
    })




  }
})