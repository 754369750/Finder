//index.js
//获取应用实例
const app = getApp()

Page({
  data: {

    userInfo: {},
    hasUserInfo: false,
    canIUse: wx.canIUse('button.open-type.getUserInfo')
  },
  //事件处理函数
  bindViewTap: function() {
    wx.navigateTo({
      url: '../logs/logs'
    })
  },
  onLoad: function() {
    let that = this;
    wx.getUserInfo({
      success: res => {
        that.setData({
          userInfo: res.userInfo,
          hasUserInfo: true
        })
      }
    })

  },
  onShow: function() {

    let that = this;
    wx.getUserInfo({
      success: res => {
        that.setData({
          userInfo: res.userInfo,
          hasUserInfo: true
        })
      }
    })



  },
  personinfoDetail: function(e) {
    wx.navigateTo({
      title: "个人信息",
      url: '../person_info/person_info'
    })
  },
  jiankongHistory: function(e) {
    wx.navigateTo({
      title: "监控历史",
      url: '../history/history'
    })
  },
  contactsDetail: function(e) {
    wx.navigateTo({
      title: "我的联系人",
      url: '../contacts/contacts'
    })
  },

  onShareAppMessage: function(res) {
    let that = this;
    let relation = '1';
    if (wx.getStorageSync('authorities').indexOf("ROLE_ADMIN") > -1) {
      relation = '0';
    }
    if (res.from === 'button') {
      // 来自页面内转发按钮
      console.log(res.target)
    }
    wx.showShareMenu({
      withShareTicket: true
    })

    return {
      title: 'Finder找到你', //分享内容
      path: '/pages/index/index?id=' + wx.getStorageSync('open_id') + '&realtion=' + relation, //分享地址
      imageUrl: '../../img/findericon.png', //分享图片
      success: function(res) {
        if (res.errMsg == 'shareAppMessage:ok') { //判断分享是否成功
          if (res.shareTickets == undefined) { //判断分享结果是否有群信息
            //分享到好友操作...
          } else {
            //分享到群操作...
            var shareTicket = res.shareTickets[0];
            wx.getShareInfo({
              shareTicket: shareTicket,
              success: function(e) {
                //当前群相关信息
                var encryptedData = e.encryptedData;
                var iv = e.iv;
              }
            })
          }
        }
      }
    }
  }
})