// pages/person_info.js
let app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    username:'',
    age:'',
    phonenumber:'',
    wechat:'',
    address:''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this;

    //获取用户信息
    wx.request({
      url: app.globalData.url + '/app-users/getpersonalinfo',
      method: 'GET',
      header: {
        'Authorization': 'Bearer ' + wx.getStorageSync('id_token'),
        'content-type': 'application/x-www-form-urlencoded'
      },
      data: {
        open_id: wx.getStorageSync('open_id'),
      },
      success: res => { 
        console.log(res);
        that.setData({
          username: res.data.name,
          age: res.data.age,
          phonenumber: res.data.phone,
          wechat: res.data.weChat,
          address: res.data.address
        });
      }
    });
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  //用户名字
  bindUsernameInput:function(e){
    this.setData({
      username: e.detail.value
    })
  },
  //用户年龄
  bindAgeInput: function (e) {
    this.setData({
      age: e.detail.value
    })
  },
  //用户手机号码
  bindPhoneInput: function (e) {
    this.setData({
      phonenumber: e.detail.value
    })
  },
  //用户微信号
  bindWechatInput: function (e) {
    this.setData({
      wechat: e.detail.value
    })
  },
  //用户通讯地址
  bindAddressInput: function (e) {
    this.setData({
      address: e.detail.value
    })
  },
  submitInfo:function(){
    var that = this;
  
    //修改用户信息
    wx.request({
      url: app.globalData.url + '/app-users',
      method: 'PUT',
      header: {
        'Authorization': 'Bearer ' + wx.getStorageSync('id_token'),
        'content-type': 'application/x-www-form-urlencoded'
      },
      data: {
        open_id: wx.getStorageSync('open_id'),
        name:that.data.username,
        age: that.data.age,
        phone: that.data.phonenumber,
        we_chat: that.data.wechat,
        address: that.data.address

      },
      success: res => {
        wx.showToast({
          title: '已完成',
          icon: 'success',
          duration: 3000
        });
        that.setData({
          username: res.data.name,
          age: res.data.age,
          phonenumber: res.data.phone,
          wechat: res.data.weChat,
          address: res.data.address
        });
      },
       fail:err =>{
        wx.showToast({
          title: '失败',
          icon: 'error',
          duration: 3000
        });
      }
    });



  }
})