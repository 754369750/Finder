var amapFile = require('../../libs/amap-wx.js'); //如：..­/..­/libs/amap-wx.js// 
let app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    src: ''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    let that = this;
    var taskid = options.taskid;
    console.log(taskid)
    that.getStaticinfo(taskid);


  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {


  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  },
  getMap: function(datalist) {
    var that = this;
    let mixnumber = datalist.length/2;
    let mid_x = '';
    let mid_y = '';
    let location ='';
    let total_x = 0;
    let total_y = 0;
    let paths ='10,0x0000ff,1,,:';
    for (var i = 0; i < datalist.length;i++){
      if (mixnumber==i){
        mid_x = datalist[i].positionX;
        mid_y = datalist[i].positionY;
        location = mid_y+','+mid_x;
      }
      var temp = datalist[i].positionY + ',' + datalist[i].positionX;
      if (i != datalist.length-1){
        temp+=';';
      }
      paths += temp;
         
    }
   
    // console.log(location);

    // console.log(paths)
    var myAmapFun = new amapFile.AMapWX({
      key: "55e2591136e5d5aec54c9b1de500014e"
    });
    wx.getSystemInfo({
      success: function(data) {
        var height = data.windowHeight;
        var width = data.windowWidth;
        var size = width + "*" + height;
        myAmapFun.getStaticmap({
          zoom: 16,
          size: size,
          scale: 2,
          location: location,
          // markers: "mid,0xFF0000,A:116.37359,39.92437;116.47359,39.92437",
          //labels: "朝阳公园,2,0,16,0xFFFFFF,0x008000:116.48482,39.94858",
          //paths: "10,0x0000ff,0.1,0x0000ff,0.7:116.31604,39.96491;116.320816,39.966606;116.321785,39.966827;116.32361,39.966957;116.39361,39.966957;116.39361,39.936957",
          paths: paths,
          success: function(data) {
            console.log(data)
            that.setData({
              src: data.url
            })
          },
          fail: function(info) {
            wx.showModal({
              title: info.errMsg
            })
          }
        })
      }
    })
  },
  getStaticinfo: function(taskid) {
    let that = this;
    wx.request({
      url: app.globalData.url + '/task-details/staticmap',
      method: 'GET',
      header: {
        'Authorization': 'Bearer ' + wx.getStorageSync('id_token'),
        'content-type': 'application/x-www-form-urlencoded'
      },
      data: {
        task_id: taskid
      },
      success: res => {
        console.log("获取静态路径信息成功");
        console.log(res)
        if (res.data.length > 0) {
          that.getMap(res.data);
        }

      }
    })
  }
})